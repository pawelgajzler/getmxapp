﻿using System;
using System.Net;
using System.Xml.Serialization;

namespace GetMX.App.Model
{
    [Serializable]
    [XmlRoot("Result")]
    public class Result
    {
        [XmlElement("Domain")]
        public string Domain { get; set; }

        [XmlElement("MailExchanger")]
        public string MailExchanger { get; set; }

        [XmlElement("MXPreference")]
        public int MxPreference { get; set; }

        [XmlElement("IPAddress")]
        public string IPAddress { get; set; }
    }

    [Serializable]
    [XmlRoot("Resultlist")]
    public class ResultArray
    {
        [XmlArray("Results")]
        public Result[] Array { get; set; }
    }
}
