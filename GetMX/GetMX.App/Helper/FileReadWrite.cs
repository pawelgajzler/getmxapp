﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using GetMX.App.Exceptions;
using GetMX.App.Interfaces;
using GetMX.App.Model;
using Microsoft.Win32;

namespace GetMX.App.Helper
{
    public class FileReadWrite : IFileReadWrite
    {
        public IEnumerable<string> GetDomains()
        {
            string path = GetDomainFilePath();
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }
            var document = new XmlDocument();
            document.Load(path);
            XmlNodeList domainsNodes = document.DocumentElement.SelectNodes("/Domains/Domain");
            if (domainsNodes.Count == 0)
            {
                throw new WrongInputFileException();
            }
            var result = new List<string>();
            foreach(XmlNode node in domainsNodes)
            {
                result.Add(node.InnerText);
            }
            return result;
        }

        public void SaveResults(IEnumerable<Result> results)
        {
            string path = GetResultFilePath();
            if (string.IsNullOrEmpty(path))
            {
                return;
            }
            var resultArray = new ResultArray { Array = results.ToArray() };
            using(var writer = new StreamWriter(path))
            {
                var serializer = new XmlSerializer(typeof(ResultArray));
                serializer.Serialize(writer, resultArray);
            }
        }

        private string GetDomainFilePath()
        {
            var sourceFileDialog = new OpenFileDialog
            {
                Filter = "XML File | *.xml"
            };
            bool? result = sourceFileDialog.ShowDialog();
            if(result.HasValue && result.Value)
            {
                return sourceFileDialog.FileName;
            }
            return string.Empty;
        }

        private string GetResultFilePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "XML File | *.xml"
            };
            bool? result = saveFileDialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                return saveFileDialog.FileName;
            }
            return string.Empty;
        }
    }
}
