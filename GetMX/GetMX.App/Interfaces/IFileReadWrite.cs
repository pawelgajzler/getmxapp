﻿using GetMX.App.Model;
using System.Collections.Generic;

namespace GetMX.App.Interfaces
{
    public interface IFileReadWrite
    {
        IEnumerable<string> GetDomains();

        void SaveResults(IEnumerable<Result> results);
    }
}
