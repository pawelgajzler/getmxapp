﻿using GetMX.App.Helper;
using GetMX.App.Interfaces;
using GetMX.App.Model;
using GetMX.App.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace GetMX.App
{
    /// <summary>
    /// Logika interakcji dla klasy App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Startup += App_Startup;
        }

        private void App_Startup(object sender, StartupEventArgs e)
        {
            IoC.Register<IDNSService, DNSService>();
            IoC.Register<IFileReadWrite, FileReadWrite>();
            IoC.Register<IAddressResolver, AddressResolver>();
            IoC.RegisterToSelf<MainViewModel>();
        }
    }
}
