﻿using GetMX.App.Helper;
using GetMX.App.Interfaces;
using GetMX.App.Model;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace GetMX.App.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private readonly IAddressResolver addressResolver;
        private readonly IFileReadWrite fileReadWrite; 

        public MainViewModel(IAddressResolver addressResolver, IFileReadWrite fileReadWrite)
        {
            this.addressResolver = addressResolver;
            this.fileReadWrite = fileReadWrite;
        }

        private string domain;

        public string Domain
        {
            get => domain;
            set => Set<string>(ref domain, value, nameof(domain));
        }

        private ObservableCollection<string> domains = new ObservableCollection<string>();

        public ObservableCollection<string> Domains
        {
            get => domains;
            set => Set<ObservableCollection<string>>(ref domains, value, nameof(domains));
        }

        private ObservableCollection<Result> results = new ObservableCollection<Result>();

        public ObservableCollection<Result> Results
        {
            get => results;
            set => Set<ObservableCollection<Result>>(ref results, value, nameof(results));
        }

        private ICommand addDomainCommand;

        public ICommand AddDomainCommand
        {
            get => addDomainCommand ?? (addDomainCommand = new RelayCommand(() => 
            {
                if (!string.IsNullOrEmpty(Domain))
                {
                    Domains.Add(Domain);
                    Domain = string.Empty;
                }
            }));
        }

        private ICommand quickResolveCommand;

        public ICommand QuickResolveCommand
        {
            get => quickResolveCommand ?? (quickResolveCommand = new RelayCommand(() =>
            {
                Results = new ObservableCollection<Result>(addressResolver.ResolveDomain(Domain));
            }));
        }

        private ICommand addDomainsFromFileCommand;

        public ICommand AddDomainsFromFileCommand
        {
            get => addDomainsFromFileCommand ?? (addDomainsFromFileCommand = new RelayCommand(() =>
            {
                try
                {
                    var result = fileReadWrite.GetDomains();
                    if(!(result is null))
                    {
                        Domains = new ObservableCollection<string>(result);
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }));
        }

        private ICommand resolveCommand;

        public ICommand ResolveCommand
        {
            get => resolveCommand ?? (resolveCommand = new RelayCommand(() =>
            {
                if(Domains.Count > 0)
                {
                    Results = new ObservableCollection<Result>(addressResolver.ResolveDomains(Domains));
                }
                else
                {
                    MessageBox.Show("Domains list is empty!");
                }
            }));
        }

        private ICommand saveCommand;

        public ICommand SaveCommand
        {
            get => saveCommand ?? (saveCommand = new RelayCommand(() =>
            {
                fileReadWrite.SaveResults(Results);
            }));
        }

        private ICommand clearCommand;

        public ICommand ClearCommand
        {
            get => clearCommand ?? (clearCommand = new RelayCommand(() =>
            {
                Domains.Clear();
            }));
        }
    }
}
