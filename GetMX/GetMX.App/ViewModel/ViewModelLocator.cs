﻿using GetMX.App.Helper;

namespace GetMX.App.ViewModel
{
    public class ViewModelLocator
    {
        public MainViewModel Main
        {
            get => IoC.Resolve<MainViewModel>();
        }
    }
}
