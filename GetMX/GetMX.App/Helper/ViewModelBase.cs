﻿using System.ComponentModel;

namespace GetMX.App.Helper
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName_)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName_));
            }
        }

        protected void Set<T>(ref T property, T value, string name)
        {
            property = value;
            RaisePropertyChanged(name);
        }
    }
}
