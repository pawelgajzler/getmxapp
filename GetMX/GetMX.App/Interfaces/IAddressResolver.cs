﻿using GetMX.App.Model;
using System.Collections.Generic;

namespace GetMX.App.Interfaces
{
    public interface IAddressResolver
    {
        IEnumerable<Result> ResolveDomain(string domain);

        IEnumerable<Result> ResolveDomains(IEnumerable<string> domains);
    }
}
