﻿using GetMX.App.Interfaces;
using System.Collections.Generic;

namespace GetMX.App.Model
{
    class AddressResolver : IAddressResolver
    {
        private readonly IDNSService dnsService;

        public AddressResolver(IDNSService dnsService)
        {
            this.dnsService = dnsService;
        }

        public IEnumerable<Result> ResolveDomain(string domain)
        {
            foreach(var result in dnsService.GetMxRecords(domain))
            {
                yield return result;
            }
        }

        public IEnumerable<Result> ResolveDomains(IEnumerable<string> domains)
        {
            foreach(string domain in domains)
            {
                foreach(var result in dnsService.GetMxRecords(domain))
                {
                    yield return result;
                }
            }
        }
    }
}
