﻿using GetMX.App.Model;
using System.Collections.Generic;

namespace GetMX.App.Interfaces
{
    public interface IDNSService
    {
        IEnumerable<Result> GetMxRecords(string domain);
    }
}
