﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GetMX.App.Exceptions
{
    public class WrongInputFileException : Exception
    {
        public WrongInputFileException() : base("Incorrect format of file")
        {
        }

        public WrongInputFileException(string message) : base(message)
        {
        }

        public WrongInputFileException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected WrongInputFileException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
